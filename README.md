# I2C Target Raspberry Pi

Python script for a Raspberry Pi running as a REM30 I2C target for data collection

## Prerequisites
- pigpio installed ([setup instructions](https://abyz.me.uk/rpi/pigpio/download.html))
- pigpio daemon running (`pigpiod`)

## Use
`./i2c.py`