#!/usr/bin/env python

import pigpio
import time

I2C_ADDR = 44
channel = 0
messages = 0

def get_latest_channel_value(channel) -> list:
    if channel == 0:
        # todo: get this val from real sensors
        val = 258    # big endian 0x0102
        buffer = [
            val >> 8 & 0xFF,     # MSB, 0x01
            val & 0xFF           # LSB, 0x02
        ]
        return buffer

def i2c(id, tick):
    global channel, messages

    messages += 1
    status, num_bytes, data = pi.bsc_i2c(I2C_ADDR)
    print("{}: status = {} num_bytes = {} data = {}".format(messages, status, num_bytes, data[0] if len(data) else None))
    if status < 0:
        print("error")
        pass
    if num_bytes > 0:
        # master wrote channel value
        channel = data[0]
        print("Received write (channel {})".format(channel))
    else:
        # master wants to read
        # todo: do something useful with channel
        print("Responding to read request (channel is {})".format(channel))
        return_buf = get_latest_channel_value(channel)
        print("".join("{:02x}".format(x) for x in return_buf))

        pi.bsc_i2c(I2C_ADDR, data=return_buf)
    print()
    print()


try:
    print("Enabling I2C callback")
    pi = pigpio.pi()
    e = pi.event_callback(pigpio.EVENT_BSC, i2c)
    pi.bsc_i2c(I2C_ADDR) # Configure BSC as I2C slave

    while True:
        pass
except KeyboardInterrupt:
    print("Disabling I2C")
    e.cancel()
    pi.bsc_i2c(0)
    pi.stop()
